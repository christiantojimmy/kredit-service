package com.wgs.kreditservice.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class PembayaranKreditResponseDto {

	private KreditResponseDto kredit;
	private int tenor;
	private String totalBayar;
	
}
