package com.wgs.kreditservice.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class TipeKreditResponseDto {

	private Long id;
	private String namaTipe;
	
}
