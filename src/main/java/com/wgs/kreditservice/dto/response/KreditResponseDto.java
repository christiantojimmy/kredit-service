package com.wgs.kreditservice.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class KreditResponseDto {
	
	private Long id;
	private TipeKreditResponseDto tipeKredit;
	private String namaBarang;
	private int lamaCicilan;
	private String totalHarga;
	
}
