package com.wgs.kreditservice.dto.request;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class TipeKreditRequestDto {

	private Long id;
	
	@NotEmpty(message = "Field namaTipe tidak boleh kosong")
	private String namaTipe;
	
}
