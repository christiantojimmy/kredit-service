package com.wgs.kreditservice.dto.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class KreditRequestDto {
	
	private Long id;
	
	@NotNull(message = "Field idTipeKredit tidak boleh kosong")
	private Long idTipeKredit;
	@NotEmpty(message = "Field namaBarang tidak boleh kosong")
	private String namaBarang;
	@NotNull(message = "Field lamaCicilan tidak boleh kosong")
	private Integer lamaCicilan;
	@NotEmpty(message = "Field totalHarga tidak boleh kosong")
	private String totalHarga;
	
}
