package com.wgs.kreditservice.dto.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class PembayaranKreditRequestDto {

	private Long id;
	
	@NotNull(message = "Field idKredit tidak boleh kosong")
	private Long idKredit;
	@NotNull(message = "Field tenor tidak boleh kosong")
	private Integer tenor;
	@NotEmpty(message = "Field totalBayar tidak boleh kosong")
	private String totalBayar;
	
}
