package com.wgs.kreditservice.repository;

import org.springframework.stereotype.Repository;

import com.wgs.base.repository.BaseRepository;
import com.wgs.kreditservice.model.PembayaranKredit;

@Repository
public interface PembayaranKreditRepository extends BaseRepository<PembayaranKredit, Long> {

}
