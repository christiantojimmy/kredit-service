package com.wgs.kreditservice.repository;

import org.springframework.stereotype.Repository;

import com.wgs.base.repository.BaseRepository;
import com.wgs.kreditservice.model.TipeKredit;

@Repository
public interface TipeKreditRepository extends BaseRepository<TipeKredit, Long> {

}
