package com.wgs.kreditservice.repository;

import org.springframework.stereotype.Repository;

import com.wgs.base.repository.BaseRepository;
import com.wgs.kreditservice.model.Kredit;

@Repository
public interface KreditRepository extends BaseRepository<Kredit, Long> {

}
