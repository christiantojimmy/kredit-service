package com.wgs.kreditservice.model;

import javax.persistence.Entity;

import com.wgs.base.model.BaseModel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter 
@Setter 
@ToString
public class TipeKredit extends BaseModel {
	
	private String namaTipe;
	
	public void setForUpdate(TipeKredit tipeKredit) {
		this.namaTipe = tipeKredit.getNamaTipe();
	}
	
}
