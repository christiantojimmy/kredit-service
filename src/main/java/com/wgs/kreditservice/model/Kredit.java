package com.wgs.kreditservice.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.wgs.base.model.BaseModel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter 
@Setter 
@ToString
public class Kredit extends BaseModel {
	
	@ManyToOne
	private TipeKredit tipeKredit;
	private String namaBarang;
	private Integer lamaCicilan;
	private BigDecimal totalHarga;
	
	public void setForUpdate(Kredit kredit) {
		this.tipeKredit = kredit.getTipeKredit();
		this.namaBarang = kredit.getNamaBarang();
		this.lamaCicilan = kredit.getLamaCicilan();
		this.totalHarga = kredit.getTotalHarga();
	}
	
}
