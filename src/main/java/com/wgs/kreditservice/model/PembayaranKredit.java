package com.wgs.kreditservice.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.wgs.base.model.BaseModel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter 
@Setter 
@ToString
public class PembayaranKredit extends BaseModel {

	@ManyToOne
	private Kredit kredit;
	private int tenor;
	private BigDecimal totalBayar;
	
	public void setForUpdate(PembayaranKredit pembayaranKredit) {
		this.kredit = pembayaranKredit.getKredit();
		this.tenor = pembayaranKredit.getTenor();
		this.totalBayar = pembayaranKredit.getTotalBayar();
	}
	
}
