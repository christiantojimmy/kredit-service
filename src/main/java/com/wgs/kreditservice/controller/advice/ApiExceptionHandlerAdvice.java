package com.wgs.kreditservice.controller.advice;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.wgs.base.response.BaseErrorMessage;
import com.wgs.base.response.BaseResponseMessage;
import com.wgs.base.response.ResponseCodeEnum;

@ControllerAdvice(annotations = {RestController.class})
public class ApiExceptionHandlerAdvice extends ResponseEntityExceptionHandler {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseEntity<?> exception(Exception ex, WebRequest request) {
        LOGGER.error("Exception Handle Advice", ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal server error");
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    	List<BaseErrorMessage> respErrors = new ArrayList<>();
    	for(FieldError error:ex.getBindingResult().getFieldErrors()) {
    		respErrors.add(BaseErrorMessage.builder()
    				.field(error.getField())
    				.message(error.getDefaultMessage())
    				.build());
    	}

         return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
        		 BaseResponseMessage.builder()
        		 .statusCode(ResponseCodeEnum.INVALID_PARAMETER.getCode())
        		 .message(ResponseCodeEnum.INVALID_PARAMETER.getDescription())
        		 .errorMessages(respErrors)
        		 .build());
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    	List<BaseErrorMessage> respErrors = new ArrayList<>();
    	for(FieldError error:ex.getBindingResult().getFieldErrors()) {
    		respErrors.add(BaseErrorMessage.builder()
    				.field(error.getField())
    				.message(error.getDefaultMessage())
    				.build());
    	}

    	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
    			BaseResponseMessage.builder()
    			.statusCode(ResponseCodeEnum.INVALID_PARAMETER.getCode())
    			.message(ResponseCodeEnum.INVALID_PARAMETER.getDescription())
    			.errorMessages(respErrors)
    			.build());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    	List<BaseErrorMessage> errors = new ArrayList<>();
    	errors.add(BaseErrorMessage.builder().field("Wgs").message(ex.getMessage()).build());

    	return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).body(
    			BaseResponseMessage.builder()
    			.statusCode(ResponseCodeEnum.INVALID_PARAMETER.getCode())
    			.message(ResponseCodeEnum.INVALID_PARAMETER.getDescription())
    			.errorMessages(errors)
    			.build());
    }
    
}
