package com.wgs.kreditservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wgs.base.response.BaseResponseMessage;
import com.wgs.base.response.ResponseCodeEnum;
import com.wgs.kreditservice.dto.request.KreditRequestDto;
import com.wgs.kreditservice.dto.response.KreditResponseDto;
import com.wgs.kreditservice.service.KreditService;

@RestController
@RequestMapping("/kredit")
public class KreditController {

	@Autowired
	private KreditService service;
	
	@GetMapping
	public ResponseEntity<?> getAll() {
		BaseResponseMessage<List<KreditResponseDto>> baseResponseMessage = new BaseResponseMessage<>();
		baseResponseMessage.setStatusCode(ResponseCodeEnum.SUCCESS);
		baseResponseMessage.setErrorMessages(null);
		baseResponseMessage.setData(service.viewAll());

		return ResponseEntity.status(HttpStatus.OK).body(baseResponseMessage);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getById(@PathVariable("id") Long id) {
		BaseResponseMessage<KreditResponseDto> baseResponseMessage = new BaseResponseMessage<>();
		KreditRequestDto request = new KreditRequestDto();
		request.setId(id);
		
		if (!service.doView(baseResponseMessage, request)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(baseResponseMessage);
		} else {
			baseResponseMessage.setStatusCode(ResponseCodeEnum.VIEW_SUCCESS);
			return ResponseEntity.status(HttpStatus.OK).body(baseResponseMessage);
		}
	}
	
	@PostMapping
	public ResponseEntity<?> create(@Valid @RequestBody KreditRequestDto request) {
		BaseResponseMessage<KreditRequestDto> baseResponseMessage = new BaseResponseMessage<>();
		
		if(!service.doSave(baseResponseMessage, request)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
		} else {
			baseResponseMessage.setStatusCode(ResponseCodeEnum.SAVE_SUCCESS.getCode());
			baseResponseMessage.setMessage("Data kredit berhasil ditambahkan");
			baseResponseMessage.setData(request);
			return ResponseEntity.status(HttpStatus.OK).body(baseResponseMessage);
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@PathVariable("id") Long id, @Valid @RequestBody KreditRequestDto requestDto) {
		BaseResponseMessage<KreditRequestDto> baseResponseMessage = new BaseResponseMessage<>();
		requestDto.setId(id);

		if (!service.doUpdate(baseResponseMessage, requestDto)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
		} else {
			baseResponseMessage.setStatusCode(ResponseCodeEnum.UPDATE_SUCCESS);
			baseResponseMessage.setMessage("Data kredit berhasil diperbaharui");
			baseResponseMessage.setData(requestDto);
			return ResponseEntity.status(HttpStatus.OK).body(baseResponseMessage);
		}
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") Long id) {
		BaseResponseMessage baseResponseMessage = new BaseResponseMessage();
		
		KreditRequestDto request = new KreditRequestDto();
		request.setId(id);
		
		if(!service.doDelete(baseResponseMessage, request)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(baseResponseMessage);
		} else {
			baseResponseMessage.setStatusCode(ResponseCodeEnum.DELETE_SUCCESS);
			baseResponseMessage.setMessage("Data kredit berhasil dihapus");
			return ResponseEntity.status(HttpStatus.OK).body(baseResponseMessage);
		}
	}
	
}
