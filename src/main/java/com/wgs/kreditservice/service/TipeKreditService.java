package com.wgs.kreditservice.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wgs.base.response.BaseErrorMessage;
import com.wgs.base.response.BaseResponseMessage;
import com.wgs.base.service.BaseService;
import com.wgs.kreditservice.dto.request.TipeKreditRequestDto;
import com.wgs.kreditservice.dto.response.TipeKreditResponseDto;
import com.wgs.kreditservice.model.TipeKredit;
import com.wgs.kreditservice.repository.TipeKreditRepository;

@Service
public class TipeKreditService implements BaseService<TipeKredit, Long, TipeKreditRequestDto, TipeKreditResponseDto> {

	@Autowired
	private TipeKreditRepository repo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	private TipeKredit base;
	private TipeKredit requestEntity;
	
	@Override
	public List<TipeKreditResponseDto> viewAll() {
		List<TipeKredit> list = repo.findAll();
		List<TipeKreditResponseDto> listDto = list.stream().map(a -> modelMapper.map(a, TipeKreditResponseDto.class)).collect(Collectors.toList());
		return listDto;
	}

	@Override
	public boolean checkExist(BaseResponseMessage baseResponseMessage, TipeKreditRequestDto request) {
		boolean isValid = true;
		
		base = repo.findById(request.getId()).orElse(null);
		
		if(base == null) {
			isValid = false;
			baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("id").message("Tidak dapat menemukan tipe kredit dengan id " + request.getId()).build());
		}
		
		return isValid;
	}
	
	
	@Override
	public boolean beforeView(BaseResponseMessage baseResponseMessage, TipeKreditRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}
	
	@Override
	public boolean view(BaseResponseMessage baseResponseMessage, TipeKreditRequestDto request) {
		baseResponseMessage.setData(modelMapper.map(base, TipeKreditResponseDto.class));

		return true;
	}

	@Override
	public boolean save(BaseResponseMessage baseResponseMessage, TipeKreditRequestDto request) {
		requestEntity = modelMapper.map(request, TipeKredit.class);
		repo.save(requestEntity);
		return true;
	}
	
	@Override
	public boolean beforeUpdate(BaseResponseMessage baseResponseMessage, TipeKreditRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}

	@Override
	public boolean update(BaseResponseMessage baseResponseMessage, TipeKreditRequestDto dto) {
		requestEntity = modelMapper.map(dto, TipeKredit.class);
		base.setForUpdate(requestEntity);
		repo.save(base);
		return true;
	}

	@Override
	public boolean beforeDelete(BaseResponseMessage baseResponseMessage, TipeKreditRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}
	
	@Override
	public boolean delete(BaseResponseMessage baseResponseMessage, TipeKreditRequestDto request) {
		repo.softDelete(request.getId());
		
		return true;
	}

}
