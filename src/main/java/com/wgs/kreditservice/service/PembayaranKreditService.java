package com.wgs.kreditservice.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wgs.base.response.BaseErrorMessage;
import com.wgs.base.response.BaseResponseMessage;
import com.wgs.base.service.BaseService;
import com.wgs.kreditservice.dto.request.PembayaranKreditRequestDto;
import com.wgs.kreditservice.dto.response.KreditResponseDto;
import com.wgs.kreditservice.dto.response.PembayaranKreditResponseDto;
import com.wgs.kreditservice.model.Kredit;
import com.wgs.kreditservice.model.PembayaranKredit;
import com.wgs.kreditservice.model.TipeKredit;
import com.wgs.kreditservice.repository.KreditRepository;
import com.wgs.kreditservice.repository.PembayaranKreditRepository;

@Service
public class PembayaranKreditService implements BaseService<Kredit, Long, PembayaranKreditRequestDto, PembayaranKreditResponseDto> {

	@Autowired
	private PembayaranKreditRepository repo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private KreditRepository kreditRepo;
	
	private PembayaranKredit base;
	private PembayaranKredit requestEntity;
	
	@Override
	public List<PembayaranKreditResponseDto> viewAll() {
		List<PembayaranKredit> list = repo.findAll();
		List<PembayaranKreditResponseDto> listDto = list.stream().map(a -> modelMapper.map(a, PembayaranKreditResponseDto.class)).collect(Collectors.toList());
		return listDto;
	}

	@Override
	public boolean checkExist(BaseResponseMessage baseResponseMessage, PembayaranKreditRequestDto request) {
		boolean isValid = true;
		
		base = repo.findById(request.getId()).orElse(null);
		
		if(base == null) {
			isValid = false;
			baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("id").message("Tidak dapat menemukan pembayaran kredit dengan id " + request.getId()).build());
		}
		
		return isValid;
	}
	
	
	@Override
	public boolean beforeView(BaseResponseMessage baseResponseMessage, PembayaranKreditRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}
	
	@Override
	public boolean view(BaseResponseMessage baseResponseMessage, PembayaranKreditRequestDto request) {
		baseResponseMessage.setData(modelMapper.map(base, PembayaranKreditResponseDto.class));

		return true;
	}
	
	@Override
	public boolean beforeSave(BaseResponseMessage baseResponseMessage, PembayaranKreditRequestDto request) {
		boolean isValid = true;
		
		requestEntity = modelMapper.map(request, PembayaranKredit.class);
		
		Kredit kredit = kreditRepo.findById(request.getIdKredit()).orElse(null);
		if(kredit == null) {
			isValid = false;
			baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("idKredit").message("Tidak dapat menemukan kredit dengan id " + request.getIdKredit()).build());
		} else {
			requestEntity.setKredit(kredit);
		}
		
		return isValid;
	}

	@Override
	public boolean save(BaseResponseMessage baseResponseMessage, PembayaranKreditRequestDto request) {
		repo.save(requestEntity);
		return true;
	}
	
	@Override
	public boolean beforeUpdate(BaseResponseMessage baseResponseMessage, PembayaranKreditRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		if(isValid) {
			requestEntity = modelMapper.map(request, PembayaranKredit.class);
			
			Kredit kredit = kreditRepo.findById(request.getIdKredit()).orElse(null);
			if(kredit == null) {
				isValid = false;
				baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("idKredit").message("Tidak dapat menemukan kredit dengan id " + request.getIdKredit()).build());
			} else {
				requestEntity.setKredit(kredit);
			}
		}
		
		return isValid;
	}

	@Override
	public boolean update(BaseResponseMessage baseResponseMessage, PembayaranKreditRequestDto dto) {
		base.setForUpdate(requestEntity);
		repo.save(base);
		return true;
	}

	@Override
	public boolean beforeDelete(BaseResponseMessage baseResponseMessage, PembayaranKreditRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}
	
	@Override
	public boolean delete(BaseResponseMessage baseResponseMessage, PembayaranKreditRequestDto request) {
		repo.softDelete(request.getId());
		
		return true;
	}

}
