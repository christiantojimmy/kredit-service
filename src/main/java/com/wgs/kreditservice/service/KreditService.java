package com.wgs.kreditservice.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wgs.base.response.BaseErrorMessage;
import com.wgs.base.response.BaseResponseMessage;
import com.wgs.base.service.BaseService;
import com.wgs.kreditservice.dto.request.KreditRequestDto;
import com.wgs.kreditservice.dto.response.KreditResponseDto;
import com.wgs.kreditservice.model.Kredit;
import com.wgs.kreditservice.model.TipeKredit;
import com.wgs.kreditservice.repository.KreditRepository;
import com.wgs.kreditservice.repository.TipeKreditRepository;

@Service
public class KreditService implements BaseService<Kredit, Long, KreditRequestDto, KreditResponseDto> {
	
	@Autowired
	private KreditRepository repo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private TipeKreditRepository tipeKreditRepo;
	
	private Kredit base;
	private Kredit requestEntity;
	
	@Override
	public List<KreditResponseDto> viewAll() {
		List<Kredit> list = repo.findAll();
		List<KreditResponseDto> listDto = list.stream().map(a -> modelMapper.map(a, KreditResponseDto.class)).collect(Collectors.toList());
		return listDto;
	}

	@Override
	public boolean checkExist(BaseResponseMessage baseResponseMessage, KreditRequestDto request) {
		boolean isValid = true;
		
		base = repo.findById(request.getId()).orElse(null);
		
		if(base == null) {
			isValid = false;
			baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("id").message("Tidak dapat menemukan kredit dengan id " + request.getId()).build());
		}
		
		return isValid;
	}
	
	
	@Override
	public boolean beforeView(BaseResponseMessage baseResponseMessage, KreditRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}
	
	@Override
	public boolean view(BaseResponseMessage baseResponseMessage, KreditRequestDto request) {
		baseResponseMessage.setData(modelMapper.map(base, KreditResponseDto.class));

		return true;
	}
	
	@Override
	public boolean beforeSave(BaseResponseMessage baseResponseMessage, KreditRequestDto request) {
		boolean isValid = true;
		
		requestEntity = modelMapper.map(request, Kredit.class);
		
		TipeKredit tipeKredit = tipeKreditRepo.findById(request.getIdTipeKredit()).orElse(null);
		if(tipeKredit == null) {
			isValid = false;
			baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("idTipeKredit").message("Tidak dapat menemukan tipe kredit dengan id " + request.getIdTipeKredit()).build());
		} else {
			requestEntity.setTipeKredit(tipeKredit);
		}
		
		return isValid;
	}

	@Override
	public boolean save(BaseResponseMessage baseResponseMessage, KreditRequestDto request) {
		repo.save(requestEntity);
		return true;
	}
	
	@Override
	public boolean beforeUpdate(BaseResponseMessage baseResponseMessage, KreditRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		if(isValid) {
			requestEntity = modelMapper.map(request, Kredit.class);
			
			TipeKredit tipeKredit = tipeKreditRepo.findById(request.getIdTipeKredit()).orElse(null);
			if(tipeKredit == null) {
				isValid = false;
				baseResponseMessage.getErrorMessages().add(BaseErrorMessage.builder().field("idTipeKredit").message("Tidak dapat menemukan tipe kredit dengan id " + request.getIdTipeKredit()).build());
			} else {
				requestEntity.setTipeKredit(tipeKredit);
			}
		}
		
		return isValid;
	}

	@Override
	public boolean update(BaseResponseMessage baseResponseMessage, KreditRequestDto dto) {
		base.setForUpdate(requestEntity);
		repo.save(base);
		return true;
	}

	@Override
	public boolean beforeDelete(BaseResponseMessage baseResponseMessage, KreditRequestDto request) {
		boolean isValid = checkExist(baseResponseMessage, request);
		
		return isValid;
	}
	
	@Override
	public boolean delete(BaseResponseMessage baseResponseMessage, KreditRequestDto request) {
		repo.softDelete(request.getId());
		
		return true;
	}

}
