package com.wgs.kreditservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KreditServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KreditServiceApplication.class, args);
	}

}
